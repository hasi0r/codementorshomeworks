﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog.BusinessLayer.BusinessModels
{
    public class ClassDayDto
    {
        public int Id { get; set; }
        public CourseLogDto CourseLog { get; set; }
        public DateTime Date { get; set; }
        public Dictionary<StudentDto, bool> Presence { get; set; }
    }
}

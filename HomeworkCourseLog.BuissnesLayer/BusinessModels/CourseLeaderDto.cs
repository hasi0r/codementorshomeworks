﻿
namespace HomeworkCourseLog.BusinessLayer.BusinessModels
{
    public class CourseLeaderDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}

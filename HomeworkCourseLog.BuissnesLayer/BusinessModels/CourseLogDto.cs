﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog.BusinessLayer.BusinessModels
{
    public class CourseLogDto
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
        public CourseLeaderDto CourseLeader { get; set; }
        public DateTime StartDate { get; set; }
        public double HomeworkPercentMinimalScore { get; set; }
        public double PresencePercentMinimalScore { get; set; }
        public List<StudentDto> ParticipatingStudents { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace HomeworkCourseLog.BusinessLayer.BusinessModels
{
    public class HomeworkDto
    {
        public int Id { get; set; }
        public CourseLogDto CourseLog { get; set; }
        public double MaxPointsPossible { get; set; }
        public Dictionary<StudentDto, double> PointsOfStudents { get; set; }
    }
}


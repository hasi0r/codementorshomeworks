﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog.BusinessLayer.BusinessModels
{
    public class ReportDto
    {
        public string CourseName { get; set; }
        public DateTime StartDate { get; set; }
        public CourseLeaderDto CourseLeader { get; set; }
        public double HomeworkPercentMinimalScore { get; set; }
        public double PresencePercentMinimalScore { get; set; }
        public int ClassDaysCount { get; set; }
        public double MaxHomeworksScorePossible { get; set; }
        public List<StudentDto> StudentsList { get; set; }
        public Dictionary<int,int> StudentsPressence { get; set; } // id, pressence
        public Dictionary<int,double> StudentsScores { get; set; } //id score
    }
}

﻿using System;

namespace HomeworkCourseLog.BusinessLayer.BusinessModels
{
    public class StudentDto
    {
        public int Id { get; set; }
        public string Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public Sexes Sex { get; set; }
    }
    public enum Sexes
    {
        Female = 1,
        Male = 2
    }
}
﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.DataLayer.Models;

namespace HomeworkCourseLog.BusinessLayer.Mappers
{
    public interface IModelsMapper
    {
        ClassDay MapClassDayDtoToClassDay(ClassDayDto classDayDt);
        CourseLeader MapCourseLeaderDtoToCourseLeader(CourseLeaderDto courseLeaderDt);
        CourseLeaderDto MapCourseLeaderToCourseLeaderDto(CourseLeader courseLeader);
        CourseLog MapCourseLogDtoToCourseLog(CourseLogDto courseLogDto);
        CourseLogDto MapCourseLogToCourseLogDto(CourseLog courseLog);
        Homework MapHomeworkDtoToHomework(HomeworkDto homeworkDt);
        Student MapStudentDtoToStudent(StudentDto studentDt);
        StudentDto MapStudentToStudentDto(Student student);
    }
}
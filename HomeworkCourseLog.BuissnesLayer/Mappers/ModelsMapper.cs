﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.DataLayer.Models;

namespace HomeworkCourseLog.BusinessLayer.Mappers
{
    public class ModelsMapper : IModelsMapper
    {
        public  Student MapStudentDtoToStudent(StudentDto studentDt)
        {
            if (studentDt == null)
            {
                return null;
            }
            var student = new Student
            {
                Id = studentDt.Id,
                Pesel = studentDt.Pesel,
                Name = studentDt.Name,
                Surname = studentDt.Surname,
                BirthDate = studentDt.BirthDate,
                Sex = (DataLayer.Models.Sexes)studentDt.Sex
            };
            return student;
        }
        public  StudentDto MapStudentToStudentDto(Student student)
        {
            if (student == null)
            {
                return null;
            }
            var studentDt = new StudentDto
            {
                Id = student.Id,
                Pesel = student.Pesel,
                Name = student.Name,
                Surname = student.Surname,
                BirthDate = student.BirthDate,
                Sex = (BusinessModels.Sexes)student.Sex
            };
            return studentDt;
        }

        public  CourseLeader MapCourseLeaderDtoToCourseLeader(CourseLeaderDto courseLeaderDt)
        {
            if (courseLeaderDt == null)
            {
                return null;
            }
            var courseLeader = new CourseLeader
            {
                Id = courseLeaderDt.Id,
                Name = courseLeaderDt.Name,
                Surname = courseLeaderDt.Surname
            };
            return courseLeader;
        }
        public  CourseLeaderDto MapCourseLeaderToCourseLeaderDto(CourseLeader courseLeader)
        {
            if (courseLeader == null)
            {
                return null;
            }
            var courseLeaderDt = new CourseLeaderDto
            {
                Id = courseLeader.Id,
                Name = courseLeader.Name,
                Surname = courseLeader.Surname
            };
            return courseLeaderDt;
        }

        public  CourseLog MapCourseLogDtoToCourseLog(CourseLogDto courseLogDto)
        {
            if (courseLogDto == null)
            {
                return null;
            }
            var courseLog = new CourseLog
            {
                Id = courseLogDto.Id,
                CourseName = courseLogDto.CourseName,
                StartDate = courseLogDto.StartDate,
                HomeworkPercentMinimalScore = courseLogDto.HomeworkPercentMinimalScore,
                PresencePercentMinimalScore = courseLogDto.PresencePercentMinimalScore
            };
            return courseLog;
        }
        public  CourseLogDto MapCourseLogToCourseLogDto(CourseLog courseLog)
        {
            if (courseLog == null)
            {
                return null;
            }
            var courseLogDt = new CourseLogDto
            {
                Id = courseLog.Id,
                CourseName = courseLog.CourseName,
                StartDate = courseLog.StartDate,
                HomeworkPercentMinimalScore = courseLog.HomeworkPercentMinimalScore,
                PresencePercentMinimalScore = courseLog.PresencePercentMinimalScore
            };
            return courseLogDt;
        }

        public  ClassDay MapClassDayDtoToClassDay(ClassDayDto classDayDt)
        {
            if (classDayDt == null)
            {
                return null;
            }
            var classDay = new ClassDay
            {
                Id = classDayDt.Id,
                Date = classDayDt.Date,
            };
            return classDay;
        }

        public  Homework MapHomeworkDtoToHomework(HomeworkDto homeworkDt)
        {
            if (homeworkDt == null)
            {
                return null;
            }
            var homework = new Homework
            {
                Id = homeworkDt.Id,
                MaxPointsPossible = homeworkDt.MaxPointsPossible
            };
            return homework;
        }
    }
}

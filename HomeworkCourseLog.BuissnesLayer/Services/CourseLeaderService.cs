﻿using System.Collections.Generic;
using System.Linq;
using HomeworkCourseLog.DataLayer.Repositories;
using HomeworkCourseLog.BusinessLayer.Mappers;
using HomeworkCourseLog.BusinessLayer.BusinessModels;
using System;

namespace HomeworkCourseLog.BusinessLayer.Services
{
    public class CourseLeaderService
    {
        private CourseLeaderRepository _courseLeaderRepository;
        private ModelsMapper _modelsMapper;

        public CourseLeaderService()
        {
            _courseLeaderRepository = new CourseLeaderRepository();
            _modelsMapper = new ModelsMapper();
        }
        
        public int AddCourseLeader(CourseLeaderDto courseLeaderDt)
        {
            var courseLeader = _modelsMapper.MapCourseLeaderDtoToCourseLeader(courseLeaderDt);
            return _courseLeaderRepository.AddCourseLeader(courseLeader);
        }

        public List<CourseLeaderDto> GetAllCourseLeaders()
        {
            return _courseLeaderRepository.GetAll()
                        .Select(x => _modelsMapper.MapCourseLeaderToCourseLeaderDto(x)).ToList();
        }

        public void UpdateCourseLeader(CourseLeaderDto updatedCourseLeader)
        {
            _courseLeaderRepository.UpdateCourseLeaderData(_modelsMapper.MapCourseLeaderDtoToCourseLeader(updatedCourseLeader));

        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using HomeworkCourseLog.DataLayer.Repositories;
using HomeworkCourseLog.BusinessLayer.Mappers;
using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.DataLayer.Models;
using System;

namespace HomeworkCourseLog.BusinessLayer.Services
{
    public class CourseLogService
    {
        private CourseLogRepository _courseLogRepository;
        private ClassDayRepository _classDayRepository;
        private HomeworkRepository _homeworkRepository;
        private ModelsMapper _modelsMapper;


        public CourseLogService()
        {
            _courseLogRepository = new CourseLogRepository();
            _classDayRepository = new ClassDayRepository();
            _homeworkRepository = new HomeworkRepository();
            _modelsMapper = new ModelsMapper();
        }

        public int AddCourseLog(CourseLogDto courseLogDt)
        {
            var courseLog = _modelsMapper.MapCourseLogDtoToCourseLog(courseLogDt);
            courseLog.CourseLeader = _modelsMapper.MapCourseLeaderDtoToCourseLeader(courseLogDt.CourseLeader);
            courseLog.StudentList = courseLogDt.ParticipatingStudents?.Select(s => _modelsMapper.MapStudentDtoToStudent(s)).ToList();

            return _courseLogRepository.AddCourselog(courseLog);
        }

        public List<CourseLogDto> GetAllCourseLogs()
        {
            var courseLogsDt = _courseLogRepository.GetAll()
                .Select(x => new CourseLogDto
                {
                    Id = x.Id,
                    CourseName = x.CourseName,
                    StartDate = x.StartDate,
                    HomeworkPercentMinimalScore = x.HomeworkPercentMinimalScore,
                    PresencePercentMinimalScore = x.PresencePercentMinimalScore,
                    CourseLeader = _modelsMapper.MapCourseLeaderToCourseLeaderDto(x.CourseLeader)
                })
                .ToList();

            return courseLogsDt;
        }

        public CourseLogDto GetCourseLogById(int courseId)
        {
            var courseLog = _courseLogRepository.GetCourseLogById(courseId);

            var courseLogDto = _modelsMapper.MapCourseLogToCourseLogDto(courseLog);

            courseLogDto.CourseLeader = _modelsMapper.MapCourseLeaderToCourseLeaderDto(courseLog.CourseLeader);
            //courseLog.StudentList.ForEach(x => courseLogDto.ParticipatingStudents.Add(ModelsMapper.MapStudentToStudentDto(x)));
            return courseLogDto;
        }

        public void AddClassDay(ClassDayDto classDayDt)
        {
            var newClassDay = _modelsMapper.MapClassDayDtoToClassDay(classDayDt);
            newClassDay.Courselog = _modelsMapper.MapCourseLogDtoToCourseLog(classDayDt.CourseLog);

            var studentsPressence = new Dictionary<Student, bool>();
            foreach (var student in classDayDt.Presence)
            {
                studentsPressence.Add(_modelsMapper.MapStudentDtoToStudent(student.Key), student.Value);
            }
            var classDayId = _classDayRepository.AddClassDay(newClassDay, studentsPressence);
        }

        public void AddHomework(HomeworkDto homeworkDt)
        {
            var newHomework = _modelsMapper.MapHomeworkDtoToHomework(homeworkDt);
            newHomework.Courselog = _modelsMapper.MapCourseLogDtoToCourseLog(homeworkDt.CourseLog);

            var studentsScores = new Dictionary<Student, double>();
            foreach (var student in homeworkDt.PointsOfStudents)
            {
                studentsScores.Add(_modelsMapper.MapStudentDtoToStudent(student.Key), student.Value);
            }
            var homeworkId = _homeworkRepository.AddHomework(newHomework, studentsScores);
        }

        public List<CourseLogDto> GetCourseLogsByStudentId(int studentId)
        {
            var courseLogs = _courseLogRepository.GetCourseLogsByStudentId(studentId);

            var courseLogsDto = new List<CourseLogDto>();
            foreach (var courseLog in courseLogs)
            {
                courseLogsDto.Add(_modelsMapper.MapCourseLogToCourseLogDto(courseLog));
            }
            return courseLogsDto;
        }

        public void RemoveStudentsFromCourse(int courseId, List<int> removedStudents)
        {
            _courseLogRepository.RemoveStudentsFromCourseLog(courseId, removedStudents);
        }

        public void UpdateCourseLogData(CourseLogDto updatedCourseLogDto)
        {
            var updatedCourseLog = _modelsMapper.MapCourseLogDtoToCourseLog(updatedCourseLogDto);
            updatedCourseLog.CourseLeader = _modelsMapper.MapCourseLeaderDtoToCourseLeader(updatedCourseLogDto.CourseLeader);
            _courseLogRepository.UpdateCourseLogData(updatedCourseLog);
        }
    }
}

﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.DataLayer.Repositories;
using HomeworkCourseLog.BusinessLayer.Mappers;
using System.Collections.Generic;
using HomeworkCourseLog.DataLayer.Repositories.Interfaces;

namespace HomeworkCourseLog.BusinessLayer.Services
{
    public class ReportService
    {
        private IStudentsRepository _studentsRepository;
        private IClassDayRepository _classDayRepository;
        private IHomeworkRepository _homeworkRepository;
        private IModelsMapper _modelsMapper;
        public ReportService()
        {
            _studentsRepository = new StudentsRepository();
            _classDayRepository = new ClassDayRepository();
            _homeworkRepository = new HomeworkRepository();
            _modelsMapper = new ModelsMapper();
        }

        public ReportService(IStudentsRepository studentsRepository, IClassDayRepository classDayRepository, IHomeworkRepository homeworkRepository, IModelsMapper modelsMapper)
        {
         
            _studentsRepository = studentsRepository;
            _classDayRepository = classDayRepository;
            _homeworkRepository = homeworkRepository;
            _modelsMapper = modelsMapper;
        }

        public ReportDto GetReport(CourseLogDto courseLogDto)
        {
            if(courseLogDto == null)
            {
                return null;
            }
               
            var studentsList = new List<StudentDto>();
            _studentsRepository.GetStudentsByCourseId(courseLogDto.Id).ForEach(x => studentsList.Add(_modelsMapper.MapStudentToStudentDto(x)));

            var studentsPressence = new Dictionary<int, int>();
            var studentsScores = new Dictionary<int, double>();

            foreach (var student in studentsList)
            {
                studentsPressence.Add(student.Id, _classDayRepository.GetStudentAttendenceOnCourse(courseLogDto.Id, student.Id));
                studentsScores.Add(student.Id, _homeworkRepository.GetStudentSummaryScoreOnCourse(courseLogDto.Id, student.Id));
            }

            var report = new ReportDto
            {
                CourseName = courseLogDto.CourseName,
                StartDate = courseLogDto.StartDate,
                HomeworkPercentMinimalScore = courseLogDto.HomeworkPercentMinimalScore,
                PresencePercentMinimalScore = courseLogDto.PresencePercentMinimalScore,
                StudentsList = studentsList,
                ClassDaysCount = _classDayRepository.GetClassDaysCountByCourseId(courseLogDto.Id),
                MaxHomeworksScorePossible = _homeworkRepository.GetHomeworkMaxScorePossibleByCourseId(courseLogDto.Id),
                StudentsPressence = studentsPressence,
                StudentsScores = studentsScores,
                CourseLeader = courseLogDto.CourseLeader

            };
            return report;
        }
    }
}

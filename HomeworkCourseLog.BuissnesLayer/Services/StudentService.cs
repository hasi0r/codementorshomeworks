﻿using System.Collections.Generic;
using System.Linq;
using HomeworkCourseLog.DataLayer.Repositories;
using HomeworkCourseLog.BusinessLayer.Mappers;
using HomeworkCourseLog.BusinessLayer.BusinessModels;
using System;

namespace HomeworkCourseLog.BusinessLayer.Services
{
    public class StudentService
    {
        private StudentsRepository _studentsRepository;
        private ModelsMapper _modelsMapper;

        public StudentService()
        {
            _studentsRepository = new StudentsRepository();
            _modelsMapper = new ModelsMapper();
        }

        public int AddStudent(StudentDto studentDt)
        {
            var student = _modelsMapper.MapStudentDtoToStudent(studentDt);
            return _studentsRepository.AddStudent(student);
        }

        public List<StudentDto> GetAllStudents()
        {
            return _studentsRepository.GetAllStudents()
                .Select(s => _modelsMapper.MapStudentToStudentDto(s)).ToList();
        }

        public List<StudentDto> GetStudentsByCourseId(int courseId)
        {
            return _studentsRepository.GetStudentsByCourseId(courseId)
                .Select(s => _modelsMapper.MapStudentToStudentDto(s)).ToList();
        }

        public void UpdateStudentData(StudentDto updatedStudentDto)
        {
            _studentsRepository.UpdateStudentData(_modelsMapper.MapStudentDtoToStudent(updatedStudentDto));
        }

        public void RemoveCourseLogFromStudent(int studentId, List<int> removedCourses)
        {
            _studentsRepository.RemoveCourseLogFromStudent(studentId, removedCourses);

        }
    }
}

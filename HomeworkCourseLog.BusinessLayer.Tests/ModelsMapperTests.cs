﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using HomeworkCourseLog.DataLayer.Models;
using System.Globalization;
using HomeworkCourseLog.BusinessLayer.Mappers;
using HomeworkCourseLog.BusinessLayer.BusinessModels;
using NUnit.Framework;

namespace HomeworkCourseLog.BusinessLayer.Tests
{
    [TestFixture]
    public class ModelsMapperTests
    {
        [Test]
        public void MapStudentToStudentDto_ValidStudent_ValidStudentDto()
        {
            //Arrange
            var student = new Student
            {
                Id = 1,
                Pesel = "66010103618",
                Name = "Test",
                Surname = "Testowy",
                BirthDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                Sex = (DataLayer.Models.Sexes)1
            };
            var modelsMapper = new ModelsMapper();
              //Act
              var result = modelsMapper.MapStudentToStudentDto(student);

            //Assert
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("66010103618", result.Pesel);
            Assert.AreEqual("Test", result.Name);
            Assert.AreEqual("Testowy", result.Surname);
            Assert.AreEqual(DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture), result.BirthDate);
            Assert.AreEqual((BusinessModels.Sexes)1, result.Sex);
        }
        [Test]
        public void MapStudentToStudentDto_Null_Null()
        {
            //Arrange
            Student student = null;
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapStudentToStudentDto(student);

            //Assert
            Assert.IsNull(result);
        }
        [Test]
        public void MapStudentToStudentDto_HalfDataStudent_ValidStudentDto()
        {
            //Arrange
            var student = new Student
            {
                Id = 1,
                Pesel = "66010103618",
                Name = "Test",
            };
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapStudentToStudentDto(student);

            //Assert
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("66010103618", result.Pesel);
            Assert.AreEqual("Test", result.Name);
            Assert.AreEqual(null, result.Surname);
            Assert.AreEqual(new DateTime(), result.BirthDate);
            Assert.AreEqual((BusinessModels.Sexes)0, result.Sex);
        }
        [Test]
        public void MapStudentDtoToStudent_ValidStudentDto_ValidStudent()
        {
            //Arrange
            var studentDto = new StudentDto
            {
                Id = 1,
                Pesel = "66010103618",
                Name = "Test",
                Surname = "Testowy",
                BirthDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                Sex = (BusinessModels.Sexes)1
            };
            var modelsMapper = new ModelsMapper();
            
            //Act
            var result = modelsMapper.MapStudentDtoToStudent(studentDto);

            //Assert
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("66010103618", result.Pesel);
            Assert.AreEqual("Test", result.Name);
            Assert.AreEqual("Testowy", result.Surname);
            Assert.AreEqual(DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture), result.BirthDate);
            Assert.AreEqual((DataLayer.Models.Sexes)1, result.Sex);
        }
        [Test]
        public void MapStudentDtoToStudent_Null_Null()
        {
            //Arrange
            StudentDto studentDto = null;
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapStudentDtoToStudent(studentDto);

            //Assert
            Assert.IsNull(result);
        }
        [Test]
        public void MapStudentDtoToStudent_HalfDataStudentDto_ValidStudent()
        {
            //Arrange
            var studentDto = new StudentDto
            {
                Id = 1,
                Pesel = "66010103618",
                Name = "Test",
            };
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapStudentDtoToStudent(studentDto);

            //Assert
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("66010103618", result.Pesel);
            Assert.AreEqual("Test", result.Name);
            Assert.AreEqual(null, result.Surname);
            Assert.AreEqual(new DateTime(), result.BirthDate);
            Assert.AreEqual((DataLayer.Models.Sexes)0, result.Sex);
        }
        [Test]
        public void MapCourseLogDtoToCourseLog_ValidCourseLogDto_ValidCourseLog()
        {
            //Arrange
            var courseLogDto = new CourseLogDto
            {
                Id = 5,
                CourseName = "Testowy",
                StartDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                HomeworkPercentMinimalScore = 65.5,
                PresencePercentMinimalScore = 50d
            };
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapCourseLogDtoToCourseLog(courseLogDto);

            //Assert
            Assert.AreEqual(5, result.Id);
            Assert.AreEqual("Testowy", result.CourseName);
            Assert.AreEqual(DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture), result.StartDate);
            Assert.AreEqual(65.5, result.HomeworkPercentMinimalScore);
            Assert.AreEqual(50d, result.PresencePercentMinimalScore);
        }
        [Test]
        public void MapCourseLogDtoToCourseLog_Null_Null()
        {
            //Arrange
            CourseLogDto courseLogDto = null;
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapCourseLogDtoToCourseLog(courseLogDto);

            //Assert
            Assert.IsNull(result);
        }
        [Test]
        public void MapCourseLogDtoToCourseLog_HalfDataCourseLogDto_ValidCourseLog()
        {
            //Arrange
            var courseLogDto = new CourseLogDto
            {
                Id = 5,
                CourseName = "Testowy",
                StartDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
            };
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapCourseLogDtoToCourseLog(courseLogDto);

            //Assert
            Assert.AreEqual(5, result.Id);
            Assert.AreEqual("Testowy", result.CourseName);
            Assert.AreEqual(DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture), result.StartDate);
            Assert.AreEqual(0.0, result.HomeworkPercentMinimalScore);
            Assert.AreEqual(0.0, result.PresencePercentMinimalScore);
        }
        [Test]
        public void MapCourseLogToCourseLogDto_ValidCourseLog_ValidCourseLogDto()
        {
            //Arrange
            var courseLog = new CourseLog
            {
                Id = 5,
                CourseName = "Testowy",
                StartDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                HomeworkPercentMinimalScore = 65.5,
                PresencePercentMinimalScore = 50d
            };
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapCourseLogToCourseLogDto(courseLog);

            //Assert
            Assert.AreEqual(5, result.Id);
            Assert.AreEqual("Testowy", result.CourseName);
            Assert.AreEqual(DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture), result.StartDate);
            Assert.AreEqual(65.5, result.HomeworkPercentMinimalScore);
            Assert.AreEqual(50d, result.PresencePercentMinimalScore);
        }
        [Test]
        public void MapCourseLogToCourseLogDto_Null_Null()
        {
            //Arrange
            CourseLog courseLog = null;
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapCourseLogToCourseLogDto(courseLog);

            //Assert
            Assert.IsNull(result);
        }
        [Test]
        public void MapCourseLogToCourseLogDto_HalfDataCourseLog_ValidCourseLogDto()
        {
            //Arrange
            var courseLog = new CourseLog
            {
                Id = 5,
                CourseName = "Testowy",
                StartDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
            };
            var modelsMapper = new ModelsMapper();

            //Act
            var result = modelsMapper.MapCourseLogToCourseLogDto(courseLog);

            //Assert
            Assert.AreEqual(5, result.Id);
            Assert.AreEqual("Testowy", result.CourseName);
            Assert.AreEqual(DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture), result.StartDate);
            Assert.AreEqual(0.0, result.HomeworkPercentMinimalScore);
            Assert.AreEqual(0.0, result.PresencePercentMinimalScore);
        }
    }
}

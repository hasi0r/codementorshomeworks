﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.DataLayer.Repositories.Interfaces;
using Moq;
using HomeworkCourseLog.BusinessLayer.BusinessModels;
using System.Globalization;
using HomeworkCourseLog.DataLayer.Models;
using System.Collections.Generic;
using HomeworkCourseLog.BusinessLayer.Mappers;
using NUnit.Framework;

namespace HomeworkCourseLog.BusinessLayer
{
    [TestFixture]
    public class ReportServiceTest
    {
        [Test]
        public void GetReport_ValidCourseLogDto_ValidReport()
        {
            //Arrange
            var courseLogDto = new CourseLogDto
            {
                Id = 1,
                CourseLeader = new CourseLeaderDto
                {
                    Id = 2,
                    Name = "Ben",
                    Surname = "Smith"
                },
                CourseName = "Testowy",
                HomeworkPercentMinimalScore = 90.0,
                PresencePercentMinimalScore = 80.0,
                StartDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture)
            };
            var studentsList = new List<Student>
            {
                new Student
                {
                    Id = 1,
                    Name = "John",
                    Surname = "Thomson",
                    BirthDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                },
                new Student
                {
                    Id = 2,
                    Name = "Steven",
                    Surname = "Johnson",
                    BirthDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                }
            };
            var studentsListDto = new List<StudentDto>
            {
                new StudentDto
                {
                    Id = 1,
                    Name = "John",
                    Surname = "Thomson",
                    BirthDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                },
                new StudentDto
                {
                    Id = 2,
                    Name = "Steven",
                    Surname = "Johnson",
                    BirthDate = DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture),
                }
            };

            var studentsRepositoryMoq = new Mock<IStudentsRepository>();
            var classDayRepositoryMoq = new Mock<IClassDayRepository>();
            var homeworkRepositoryMoq = new Mock<IHomeworkRepository>();
            var modelsMapperMoq = new Mock<IModelsMapper>();

            studentsRepositoryMoq.Setup(x => x.GetStudentsByCourseId(1)).Returns(studentsList);

            modelsMapperMoq.Setup(x => x.MapStudentToStudentDto(studentsList[0])).Returns(studentsListDto[0]);
            modelsMapperMoq.Setup(x => x.MapStudentToStudentDto(studentsList[1])).Returns(studentsListDto[1]);

            classDayRepositoryMoq.Setup(x => x.GetStudentAttendenceOnCourse(1, 1)).Returns(10);
            classDayRepositoryMoq.Setup(x => x.GetStudentAttendenceOnCourse(1, 2)).Returns(15);
            classDayRepositoryMoq.Setup(x => x.GetClassDaysCountByCourseId(1)).Returns(20);

            homeworkRepositoryMoq.Setup(x => x.GetStudentSummaryScoreOnCourse(1, 1)).Returns(78.0);
            homeworkRepositoryMoq.Setup(x => x.GetStudentSummaryScoreOnCourse(1, 2)).Returns(66.0);
            homeworkRepositoryMoq.Setup(x => x.GetHomeworkMaxScorePossibleByCourseId(1)).Returns(90);


            var reportService = new ReportService(studentsRepositoryMoq.Object,
                                                  classDayRepositoryMoq.Object,
                                                  homeworkRepositoryMoq.Object,
                                                  modelsMapperMoq.Object);
            //Act
            var result = reportService.GetReport(courseLogDto);
            //Assert

            Assert.AreEqual("Testowy", result.CourseName);
            Assert.AreEqual(DateTime.ParseExact("10.10.2017", "dd.MM.yyyy", CultureInfo.InvariantCulture), result.StartDate);
            Assert.AreEqual(90.0, result.HomeworkPercentMinimalScore);
            Assert.AreEqual(80.0, result.PresencePercentMinimalScore);
            Assert.AreEqual(20, result.ClassDaysCount);
            Assert.AreEqual(90, result.MaxHomeworksScorePossible);
            Assert.AreEqual(10, result.StudentsPressence[1]);
            Assert.AreEqual(15, result.StudentsPressence[2]);
            Assert.AreEqual(78.0, result.StudentsScores[1]);
            Assert.AreEqual(66.0, result.StudentsScores[2]);
            Assert.AreEqual("Testowy", result.CourseName);
            Assert.AreEqual("Ben", result.CourseLeader.Name);
            Assert.AreEqual("Smith", result.CourseLeader.Surname);
            Assert.AreEqual(2, result.CourseLeader.Id);
            Assert.AreEqual(2, result.CourseLeader.Id);
            Assert.AreEqual(2, result.CourseLeader.Id);
            Assert.AreEqual(studentsListDto[0].Id, result.StudentsList[0].Id);
            Assert.AreEqual(studentsListDto[0].Name, result.StudentsList[0].Name);
            Assert.AreEqual(studentsListDto[0].Surname, result.StudentsList[0].Surname);
            Assert.AreEqual(studentsListDto[0].BirthDate, result.StudentsList[0].BirthDate);
            Assert.AreEqual(studentsListDto[1].Id, result.StudentsList[1].Id);
            Assert.AreEqual(studentsListDto[1].Name, result.StudentsList[1].Name);
            Assert.AreEqual(studentsListDto[1].Surname, result.StudentsList[1].Surname);
            Assert.AreEqual(studentsListDto[1].BirthDate, result.StudentsList[1].BirthDate);
        }

        [Test]
        public void GetReport_Null_Null()
        {
            //Arrange
            var courseLogDto = new CourseLogDto();
            courseLogDto = null;

            var studentsRepositoryMoq = new Mock<IStudentsRepository>();
            var classDayRepositoryMoq = new Mock<IClassDayRepository>();
            var homeworkRepositoryMoq = new Mock<IHomeworkRepository>();
            var modelsMapperMoq = new Mock<IModelsMapper>();

            var reportService = new ReportService(studentsRepositoryMoq.Object,
                                                  classDayRepositoryMoq.Object,
                                                  homeworkRepositoryMoq.Object,
                                                  modelsMapperMoq.Object);
            //Act
            var result = reportService.GetReport(null);
            //Assert

            Assert.IsNull(result);
        }
    }
}

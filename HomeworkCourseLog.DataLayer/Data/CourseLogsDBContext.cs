﻿using System.Data.Entity;
using System.Configuration;
using HomeworkCourseLog.DataLayer.Models;

namespace HomeworkCourseLog.DataLayer.Data
{
    public class CourseLogsDBContext : DbContext
    {
        public CourseLogsDBContext() :base(GetConnectionString())
        { }

        public DbSet<ClassDay> ClassDays { get; set; }
        public DbSet<Homework> Homeworks { get; set; }
        public DbSet<CourseLeader> CourseLeaders { get; set; }
        public DbSet<CourseLog> CourseLogs { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentHomework> StudentHomeworks { get; set; }
        public DbSet<StudentPressence> StudentPressence { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["CourselogsSqlDb"].ConnectionString;
        }
    }
}

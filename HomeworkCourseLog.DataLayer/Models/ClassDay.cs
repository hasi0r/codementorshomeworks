﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog.DataLayer.Models
{
    public class ClassDay
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public List<StudentPressence> Presence { get; set; }
        public CourseLog Courselog { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace HomeworkCourseLog.DataLayer.Models
{
    public class CourseLeader
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<CourseLog> Courselog { get; set; }
    }
}

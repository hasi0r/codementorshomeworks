﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog.DataLayer.Models
{
    public class CourseLog
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
        public CourseLeader CourseLeader { get; set; }
        public DateTime StartDate { get; set; }
        public double HomeworkPercentMinimalScore { get; set; }
        public double PresencePercentMinimalScore { get; set; }
        public List<Student> StudentList { get; set; }
        public List<ClassDay> ClassDays { get; set; }
        public List<Homework> Homeworks { get; set; }
    }
}

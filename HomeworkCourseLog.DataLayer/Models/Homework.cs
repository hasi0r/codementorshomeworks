﻿using System.Collections.Generic;

namespace HomeworkCourseLog.DataLayer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public double MaxPointsPossible { get; set; }
        public List<StudentHomework> StudentsPoints { get; set; }
        public CourseLog Courselog { get; set; }
    }
}

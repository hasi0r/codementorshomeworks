﻿using System;
using System.Collections.Generic;

namespace HomeworkCourseLog.DataLayer.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public Sexes Sex { get; set; }
        public List<CourseLog> CourseLogs { get; set; }
        public List<StudentHomework> Homeworks { get; set; }
        public List<StudentPressence> ClassDays { get; set; }
    }
    public enum Sexes
    {
        Female = 1,
        Male = 2
    }
}

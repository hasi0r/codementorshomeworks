﻿namespace HomeworkCourseLog.DataLayer.Models
{
    public class StudentHomework
    {
        public int Id { get; set; }
        public Homework Homework { get; set; }
        public Student Student { get; set; }
        public double Score { get; set; }
    }
}

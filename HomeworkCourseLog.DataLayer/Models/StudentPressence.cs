﻿namespace HomeworkCourseLog.DataLayer.Models
{
    public class StudentPressence
    {
        public int Id { get; set; }
        public Student Student { get; set; }
        public ClassDay ClassDay { get; set; }
        public bool Pressence { get; set; }
    }
}

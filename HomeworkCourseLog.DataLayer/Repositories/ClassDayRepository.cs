﻿using HomeworkCourseLog.DataLayer.Models;
using HomeworkCourseLog.DataLayer.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using HomeworkCourseLog.DataLayer.Repositories.Interfaces;

namespace HomeworkCourseLog.DataLayer.Repositories
{
    public class ClassDayRepository : IClassDayRepository
    {
        public int AddClassDay(ClassDay classDay, Dictionary<Student, bool> studentsPressence)
        {
            ClassDay addedClassDay;

            using (var dbContext = new CourseLogsDBContext())
            {
                if (classDay.Courselog != null && !dbContext.CourseLogs.Local.Contains(classDay.Courselog))
                {
                    dbContext.CourseLogs.Attach(classDay.Courselog);
                    dbContext.Entry(classDay.Courselog).State = EntityState.Modified;
                }

                addedClassDay = dbContext.ClassDays.Add(classDay);
                dbContext.SaveChanges();
            }

            if (addedClassDay == null)
            {
                return 0;
            }
            AddStudentsPressence(addedClassDay.Id, studentsPressence);
            return addedClassDay.Id;
        }
        private void AddStudentsPressence(int classDayId, Dictionary<Student, bool> studentsPressence)
        {
            if (studentsPressence.Count == 0)
            {
                return;
            }

            using (var dbContext = new CourseLogsDBContext())
            {
                var classDay = dbContext.ClassDays.Where(c => c.Id == classDayId).First();

                foreach (var student in studentsPressence)
                {
                    var studentPressence = new StudentPressence
                    {
                        ClassDay = classDay,
                        Student = dbContext.Students.Where(c => c.Id == student.Key.Id).First(),
                        Pressence = student.Value
                    };
                    dbContext.StudentPressence.Add(studentPressence);
                }
                dbContext.SaveChanges();
            }
        }

        public int GetClassDaysCountByCourseId(int courseId)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.ClassDays
                    .Where(x => x.Courselog.Id == courseId)
                    .Count();
            }
        }

        public int GetStudentAttendenceOnCourse(int courseId, int studentId)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.StudentPressence
                       .Where(x => (x.ClassDay.Courselog.Id == courseId && x.Student.Id == studentId && x.Pressence == true))
                       .Count();
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using HomeworkCourseLog.DataLayer.Models;
using HomeworkCourseLog.DataLayer.Data;
using System;
using HomeworkCourseLog.DataLayer.Repositories.Interfaces;

namespace HomeworkCourseLog.DataLayer.Repositories
{
    public class CourseLeaderRepository : ICourseLeaderRepository
    {
        public int AddCourseLeader(CourseLeader courseLeader)
        {
            CourseLeader addedCourseLeader;

            using (var dbContext = new CourseLogsDBContext())
            {
                addedCourseLeader = dbContext.CourseLeaders.Add(courseLeader);
                dbContext.SaveChanges();
            }
            return addedCourseLeader.Id;
        }

        public List<CourseLeader> GetAll()
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.CourseLeaders.ToList();
            }
        }

        public void UpdateCourseLeaderData(CourseLeader courseLeader)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                var existingCourseLeader = dbContext.CourseLeaders
                                                    .Where(x => x.Id == courseLeader.Id)
                                                    .SingleOrDefault();
                dbContext.Entry(existingCourseLeader).CurrentValues.SetValues(courseLeader);
                dbContext.SaveChanges();
            }
        }
    }
}

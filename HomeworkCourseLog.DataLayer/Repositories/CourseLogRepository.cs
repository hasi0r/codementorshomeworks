﻿using HomeworkCourseLog.DataLayer.Models;
using HomeworkCourseLog.DataLayer.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System;

namespace HomeworkCourseLog.DataLayer.Repositories
{
    public class CourseLogRepository
    {
        public int AddCourselog(CourseLog courseLog)
        {
            CourseLog addedCourseLog;

            using (var dbContext = new CourseLogsDBContext())
            {
                if (courseLog.CourseLeader != null && !dbContext.CourseLeaders.Local.Contains(courseLog.CourseLeader))
                {
                    dbContext.CourseLeaders.Attach(courseLog.CourseLeader);
                    dbContext.Entry(courseLog.CourseLeader).State = EntityState.Modified;
                }

                if (courseLog.StudentList != null)
                {
                    foreach (var student in courseLog.StudentList)
                    {
                        if (!dbContext.Students.Local.Contains(student))
                        {
                            dbContext.Students.Attach(student);
                            dbContext.Entry(student).State = EntityState.Modified;
                        }
                    }
                }

                addedCourseLog = dbContext.CourseLogs.Add(courseLog);
                dbContext.SaveChanges();
            }
            return addedCourseLog.Id;
        }

        public List<CourseLog> GetAll()
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.CourseLogs
                                .Include(x => x.CourseLeader)
                                .ToList();
            }
        }

        public CourseLog GetCourseLogById(int courseId)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.CourseLogs.Include(x => x.CourseLeader)
                                           .Include(x => x.StudentList)
                                           .Where(x => x.Id == courseId)
                                           .First();
            }
        }

        public void UpdateCourseLogData(CourseLog updatedCourseLog)
        {
            var newCourseLeaderId = updatedCourseLog.CourseLeader.Id;

            using (var dbContext = new CourseLogsDBContext())
            {
                var existingCourseLog = dbContext.CourseLogs
                                                 .Where(x => x.Id == updatedCourseLog.Id)
                                                 .Include(x => x.CourseLeader)
                                                 .SingleOrDefault();

                var newCourseLeader = dbContext.CourseLeaders.Where(x => x.Id == newCourseLeaderId).First();

                dbContext.Entry(existingCourseLog).CurrentValues.SetValues(updatedCourseLog);
                existingCourseLog.CourseLeader = newCourseLeader;
                dbContext.SaveChanges();
            }
        }

        public void RemoveStudentsFromCourseLog(int courseId, List<int> removedStudents)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                var existingCourseLog = dbContext.CourseLogs
                                                 .Where(x => x.Id == courseId)
                                                 .Include(x => x.StudentList)
                                                 .SingleOrDefault();

                dbContext.Entry(existingCourseLog).CurrentValues.SetValues(existingCourseLog);
                foreach (var student in existingCourseLog.StudentList.ToList())
                {
                    if (removedStudents.Contains(student.Id))
                    {
                        existingCourseLog.StudentList.Remove(student);
                        //  dbContext.Students.Remove(student);
                    }
                }
                dbContext.SaveChanges();
            }
        }

        public List<CourseLog> GetCourseLogsByStudentId(int studentId)
        {
            var studentCourseLogsList = new List<CourseLog>();
            using (var dbContext = new CourseLogsDBContext())
            {
                var courseLogsList = dbContext.CourseLogs
                                              .Include(x => x.StudentList)
                                              .ToList();

                foreach (var course in courseLogsList)
                {
                    foreach (var st in course.StudentList)
                    {
                        if (st.Id == studentId)
                        {
                            studentCourseLogsList.Add(course);
                        }
                    }
                }
            }
            return studentCourseLogsList;
        }
    }
}

﻿using HomeworkCourseLog.DataLayer.Models;
using HomeworkCourseLog.DataLayer.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using HomeworkCourseLog.DataLayer.Repositories.Interfaces;

namespace HomeworkCourseLog.DataLayer.Repositories
{
    public class HomeworkRepository : IHomeworkRepository
    {
        public int AddHomework(Homework homework, Dictionary<Student, double> studentsHomeworks)
        {
            Homework addedHomework;

            using (var dbContext = new CourseLogsDBContext())
            {
                if (homework.Courselog != null & !dbContext.CourseLogs.Local.Contains(homework.Courselog))
                {
                    dbContext.CourseLogs.Attach(homework.Courselog);
                    dbContext.Entry(homework.Courselog).State = EntityState.Modified;
                }
                addedHomework = dbContext.Homeworks.Add(homework);
                dbContext.SaveChanges();
            }

            if (addedHomework == null)
            {
                return 0;
            }
            AddStudentsHomeworkScore(addedHomework.Id, studentsHomeworks);
            return addedHomework.Id;
        }
        private void AddStudentsHomeworkScore(int homeworkId, Dictionary<Student, double> studentsHomeworks)
        {
            if (studentsHomeworks.Count == 0)
            {
                return;
            }

            using (var dbContext = new CourseLogsDBContext())
            {
                var homework = dbContext.Homeworks.Where(c => c.Id == homeworkId).First();

                foreach (var student in studentsHomeworks)
                {

                    var studentHomework = new StudentHomework
                    {
                        Homework = homework,
                        Student = dbContext.Students.Where(c => c.Id == student.Key.Id).First(),
                        Score = student.Value
                    };
                    dbContext.StudentHomeworks.Add(studentHomework);
                }
                dbContext.SaveChanges();
            }
        }

        public double GetHomeworkMaxScorePossibleByCourseId(int courseId)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.Homeworks
                    .Where(x => x.Courselog.Id == courseId)
                    .Select(x => x.MaxPointsPossible)
                    .DefaultIfEmpty(0)
                    .Sum();
            }
        }

        public double GetStudentSummaryScoreOnCourse(int courseId, int studentId)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.StudentHomeworks
                       .Where(x => (x.Homework.Courselog.Id == courseId && x.Student.Id == studentId))
                       .Select(x => x.Score).DefaultIfEmpty(0)
                       .Sum();
            }
        }
    }
}

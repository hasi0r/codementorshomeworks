﻿using System.Collections.Generic;
using HomeworkCourseLog.DataLayer.Models;

namespace HomeworkCourseLog.DataLayer.Repositories.Interfaces
{
    public interface IClassDayRepository
    {
        int AddClassDay(ClassDay classDay, Dictionary<Student, bool> studentsPressence);
        int GetClassDaysCountByCourseId(int courseId);
        int GetStudentAttendenceOnCourse(int courseId, int studentId);
    }
}
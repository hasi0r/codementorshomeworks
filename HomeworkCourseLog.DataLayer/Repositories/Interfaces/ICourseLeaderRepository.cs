﻿using System.Collections.Generic;
using HomeworkCourseLog.DataLayer.Models;

namespace HomeworkCourseLog.DataLayer.Repositories.Interfaces
{
    public interface ICourseLeaderRepository
    {
        int AddCourseLeader(CourseLeader courseLeader);
        List<CourseLeader> GetAll();
        void UpdateCourseLeaderData(CourseLeader courseLeader);
    }
}
﻿using System.Collections.Generic;
using HomeworkCourseLog.DataLayer.Models;

namespace HomeworkCourseLog.DataLayer.Repositories.Interfaces
{
    public interface IHomeworkRepository
    {
        int AddHomework(Homework homework, Dictionary<Student, double> studentsHomeworks);
        double GetHomeworkMaxScorePossibleByCourseId(int courseId);
        double GetStudentSummaryScoreOnCourse(int courseId, int studentId);
    }
}
﻿using System.Collections.Generic;
using HomeworkCourseLog.DataLayer.Models;

namespace HomeworkCourseLog.DataLayer.Repositories.Interfaces
{
    public interface IStudentsRepository
    {
        int AddStudent(Student student);
        List<Student> GetAllStudents();
        List<Student> GetStudentsByCourseId(int courseId);
        void RemoveCourseLogFromStudent(int studentId, List<int> removedCourses);
        void UpdateStudentData(Student student);
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using HomeworkCourseLog.DataLayer.Models;
using HomeworkCourseLog.DataLayer.Data;
using System;
using System.Data.Entity;
using HomeworkCourseLog.DataLayer.Repositories.Interfaces;

namespace HomeworkCourseLog.DataLayer.Repositories
{
    public class StudentsRepository : IStudentsRepository
    {
        public int AddStudent(Student student)
        {
            Student addedStudent;

            using (var dbContext = new CourseLogsDBContext())
            {
                addedStudent = dbContext.Students.Add(student);
                dbContext.SaveChanges();
            }
            return addedStudent.Id;
        }

        public List<Student> GetAllStudents()
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.Students.ToList();
            }
        }
        public List<Student> GetStudentsByCourseId(int courseId)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                return dbContext.Students
                    .Where(x => x.CourseLogs.Any(c => c.Id == courseId))
                    .ToList();
            }
        }

        public void UpdateStudentData(Student student)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                var existingStudent = dbContext.Students
                                               .Where(x => x.Id == student.Id)
                                               .SingleOrDefault();
                dbContext.Entry(existingStudent).CurrentValues.SetValues(student);
                dbContext.SaveChanges();
            }
        }

        public void RemoveCourseLogFromStudent(int studentId, List<int> removedCourses)
        {
            using (var dbContext = new CourseLogsDBContext())
            {
                var existingStudent = dbContext.Students
                                               .Where(x => x.Id == studentId)
                                               .Include(x => x.CourseLogs)
                                               .SingleOrDefault();

                dbContext.Entry(existingStudent).CurrentValues
                                                .SetValues(existingStudent);
                foreach (var course in existingStudent.CourseLogs.ToList())
                {
                    if(removedCourses.Contains(course.Id))
                    {
                        existingStudent.CourseLogs.Remove(course);
                    }
                }
                dbContext.SaveChanges();
            }
        }
    }
}

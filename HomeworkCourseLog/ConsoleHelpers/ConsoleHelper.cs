﻿using System;

namespace HomeworkCourseLog.ConsoleHelpers
{
    class ConsoleHelper
    {
        public static void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
        public static void ClearConsole()
        {
            Console.Clear();
        }
        public static void WaitForEnter()
        {
            Console.WriteLine("Aby przejść dalej wciśnij Enter...");
            Console.ReadLine();
        }
    }
}

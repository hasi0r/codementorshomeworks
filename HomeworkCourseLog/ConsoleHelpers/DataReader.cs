﻿using System;
using System.Globalization;
using System.Linq;
using HomeworkCourseLog.BusinessLayer.BusinessModels;

namespace HomeworkCourseLog.ConsoleHelpers
{
    internal class DataReader
    {
        private readonly int[] _peselMultipliers = { 1, 3, 7, 9, 1, 3, 7, 9, 1, 3 };

        public int ReadIntNumber(string message)
        {
            Console.Write($"- {message}: ");
            var input = Console.ReadLine();
            int result;
            while (!(int.TryParse(input, out result)))
            {
                Console.Write("Nieprawidłowe dane, podaj liczbę całkowitą: ");
                input = Console.ReadLine();
            }
            return result;
        }
        public int ReadIntNumber(int min, int max, string message)
        {
            Console.Write($"- {message}: ");
            string input = Console.ReadLine();
            int result;
            while (!(int.TryParse(input, out result)) || result < min || result > max)
            {
                Console.Write($"Podaj liczbę całkowitą w zakresie od {min} do {max}: ");
                input = Console.ReadLine();
            }
            return result;
        }
        public bool ReadBoolean(string message)
        {
            var input = ReadIntNumber(0, 1, message).ToString();

            if (input == "1")
            {
                input = "true";
            }
            else
            {
                input = "false";
            }
            bool result = Convert.ToBoolean(input);
            return result;
        }
        public double ReadDoubleNumber(string message)
        {
            Console.Write($"- {message}: ");
            var input = Console.ReadLine();
            double result;
            while (!(double.TryParse(input, out result)))
            {
                Console.Write("Nieprawidłowe dane, podaj liczbę: ");
                input = Console.ReadLine();
            }
            return result;
        }
        public double ReadDoubleNumber(double min, double max, string message)
        {
            Console.Write($"- {message}: ");
            var input = Console.ReadLine();
            double result;
            while (!(double.TryParse(input, out result)) || result < min || result > max)
            {
                Console.Write($"Podaj liczbę w zakresie od {min} do {max}: ");
                input = Console.ReadLine();
            }
            return result;
        }
        public DateTime ReadDateTime(string message)
        {
            var format = "dd.MM.yyyy";
            Console.Write($"- {message}({format}): ");
            var input = Console.ReadLine();
            DateTime result;
            while (!(DateTime.TryParseExact(input, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out result)))
            {
                Console.Write($"Podaj poprawny format daty({format}): ");
                input = Console.ReadLine();
            }
            return result;
        }
        public string ReadString(string message)
        {
            Console.Write($"- {message}: ");
            return Console.ReadLine();
        }
        public Sexes ReadSex(string message)
        {
            UserMenu sexMenu = new UserMenu(new string[] { "Kobieta","Mężczyzna" });
            return (Sexes)sexMenu.AskUser($"- {message}: ");
        }
        
        /// <summary>
        /// returns validated pesel number given by console user
        /// </summary>
        /// <returns></returns>
        public string ReadPesel(string message)
        {
            Console.Write($"- {message}: ");
            string input = Console.ReadLine();
            while (input.Length != 11 || !input.All(char.IsDigit) || !ValidatePesel(input))
            {
                Console.Write("Nieprawidłowe dane, podaj porawny nr Pesel: ");
                input = Console.ReadLine();
            }
            return input;
        }
        private bool ValidatePesel(string pesel)
        {
            bool result = (CountControlSumForPesel(pesel) == (int)char.GetNumericValue(pesel[10]));
            return result;
        }
        private int CountControlSumForPesel(string pesel)
        {
            int controlSum = 0;
            for (int i = 0; i < _peselMultipliers.Length; i++)
            {

                controlSum += (int)char.GetNumericValue(pesel[i]) * _peselMultipliers[i];
            }
            int rest = controlSum % 10;
            rest = 10 - rest;
            rest %= 10;
            return rest;
        }
        
    }
}
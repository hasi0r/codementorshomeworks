﻿using HomeworkCourseLog.MenuOptions.Interfaces;
using System;
using System.Linq;

namespace HomeworkCourseLog.ConsoleHelpers
{
    internal class OptionMenu 
    {
        private readonly IMenuOption[] _menuOptions;

        public OptionMenu(IMenuOption[] options)
        {
            _menuOptions = options.ToArray();
        }
      
        public int AskUser(string message)
        {
            Console.WriteLine(message);
            PrintOptions();
            int choice = GetChoice();
            return choice;
        }
        
        private int GetChoice()
        {
            int result;
            string input = Console.ReadLine();
            while (!(int.TryParse(input, out result)) || result  <1 || result> _menuOptions.Length)
            {
                Console.WriteLine("Nieprawidłowe dane, podaj liczbę z zakresu opcji");
                input = Console.ReadLine();
            }
            return result;
        }

        /// <summary>
        /// Print optinos in format: 1 - option[0]... i+1 - option[i]
        /// </summary>
        private void PrintOptions()
        {
            for (int i = 0; i < _menuOptions.Length; i++)
            {
                Console.WriteLine($"{i + 1} - {_menuOptions[i].OptionDescription}");
            }
        }
    }
}

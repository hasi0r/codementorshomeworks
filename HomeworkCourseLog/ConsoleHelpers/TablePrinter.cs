﻿using System;

namespace HomeworkCourseLog.ConsoleHelpers
{
    class TablePrinter
    {
        public static void PrintLine(int width)
        {
            Console.WriteLine(new string('-', width));
        }
        public static void PrintRow(string[] columns, int tableWidth)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
            {
                row += AlignCenter(column, width) + "|";
            }
            Console.WriteLine(row);
        }
        private static string AlignCenter(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }
    }
}

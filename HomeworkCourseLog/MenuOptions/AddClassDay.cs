﻿using HomeworkCourseLog.MenuOptions.Interfaces;
using System.Collections.Generic;
using HomeworkCourseLog.TempDataHolders;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.BusinessLayer.Services;

namespace HomeworkCourseLog.MenuOptions
{
    class AddClassDay : IMenuOption
    {
        private StudentService _studentService;
        private CourseLogService _courseLogService;
        private DataReader _dataReader;

        public AddClassDay()
        {
            _studentService = new StudentService();
            _courseLogService = new CourseLogService();
            _dataReader = new DataReader();
            OptionDescription = "Dodaj dzień zajęć";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            if (ProgramTempData.GetActiveCourseLog() == null)
            {
                ConsoleHelper.ShowMessage("Najpierw wybierz kurs.");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return;
            }
            var courseStudents = _studentService.GetStudentsByCourseId(ProgramTempData.GetActiveCourseLog().Id);

            var classDayDt = new ClassDayDto
            {
                Date = _dataReader.ReadDateTime("Podaj datę"),
                CourseLog = ProgramTempData.GetActiveCourseLog(),
                Presence = CheckPressence(courseStudents)
            };
            _courseLogService.AddClassDay(classDayDt);
        }
        private Dictionary<StudentDto, bool> CheckPressence(List<StudentDto> students)
        {
            var presence = new Dictionary<StudentDto, bool>();
            foreach (var student in students)
            {
                presence.Add(student, new DataReader().ReadBoolean($"Czy {student.Name} {student.Surname}, jest obecny?(1 - tak, 0 - nie)"));
            }
            return presence;
        }
    }
}

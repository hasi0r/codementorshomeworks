﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;

namespace HomeworkCourseLog.MenuOptions
{
    class AddCourseLeader : IMenuOption
    {
        private CourseLeaderService _courseLeaderService;
        private DataReader _dataReader;

        public AddCourseLeader()
        {
            _courseLeaderService = new CourseLeaderService();
            _dataReader = new DataReader();
            OptionDescription = "Dodaj prowadzącego kurs";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            var courseLeaderDt = new CourseLeaderDto
            {
                Name = _dataReader.ReadString("Podaj imię Prowadzącego"),
                Surname = _dataReader.ReadString("Podaj nazwisko studenta"),
            };
            _courseLeaderService.AddCourseLeader(courseLeaderDt);
        }
    }
}

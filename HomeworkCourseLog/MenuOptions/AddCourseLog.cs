﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace HomeworkCourseLog.MenuOptions
{
    internal class AddCourseLog : IMenuOption
    {
        private CourseLeaderService _courseLeaderService;
        private StudentService _studentService;
        private CourseLogService _courseLogService;
        private DataReader _dataReader;

        public AddCourseLog()
        {
            _courseLeaderService = new CourseLeaderService();
            _studentService = new StudentService();
            _courseLogService = new CourseLogService();
            _dataReader = new DataReader();
            OptionDescription = "Dodaj kurs";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            var courseleaderDt = GetCourseLeader();
            if (courseleaderDt == null) return;

            var courseLogDt = AskUserForCourseData();

            var choosenStudents = GetParticipatingStudents();
            if (choosenStudents == null) return;
            courseLogDt.CourseLeader = courseleaderDt;
            courseLogDt.ParticipatingStudents = choosenStudents;

            _courseLogService.AddCourseLog(courseLogDt);
        }

        private CourseLogDto AskUserForCourseData()
        {
            return new CourseLogDto
            {
                CourseName = _dataReader.ReadString("Podaj nazwę kursu"),
                StartDate = _dataReader.ReadDateTime("Podaj datę rozpoczęcia kursu"),
                HomeworkPercentMinimalScore = _dataReader.ReadDoubleNumber(0, 100, "Podaj próg zaliczania prac domowyc(w procentach)"),
                PresencePercentMinimalScore = _dataReader.ReadDoubleNumber(0, 100, "Podaj próg obecności(w procentach)")
            };
        }
        private CourseLeaderDto GetCourseLeader()
        {
            var validCourseLeaders = _courseLeaderService.GetAllCourseLeaders();
            if (validCourseLeaders.Count == 0)
            {
                ConsoleHelper.ShowMessage("Najpierw dodaj prowadzącego aby dodać kurs.");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return null;
            }

            ConsoleHelper.ShowMessage("Dostępni prowadzący:");
            validCourseLeaders.ForEach(x => ConsoleHelper.ShowMessage($"{x.Id} - {x.Name} {x.Surname}"));

            CourseLeaderDto courseleaderDt;
            while (true)
            {
                int choosedCourseLeaderId = _dataReader.ReadIntNumber("Podaj Id Prowadzącego");
                courseleaderDt = validCourseLeaders.SingleOrDefault(x => x.Id == choosedCourseLeaderId);

                if (courseleaderDt != null)
                {
                    break;
                }
                ConsoleHelper.ShowMessage($"Nie ma prowadzącego o nr {choosedCourseLeaderId}. Spróbuj ponownie");
            }
            return courseleaderDt;
        }
        private List<StudentDto> GetParticipatingStudents()
        {
            var validStudents = _studentService.GetAllStudents();
            if (validStudents.Count == 0)
            {
                ConsoleHelper.ShowMessage("Najpierw dodaj studentów aby dodać kurs.");
                return null;
            }

            var numberOfStudents = GetNumberOfStudentsFromUser(validStudents);
            var choosenStudents = SetListOfStudents(validStudents, numberOfStudents);
            return choosenStudents;
        }

        private int GetNumberOfStudentsFromUser(List<StudentDto> validStudents)
        {
            int numberOfStudents;
            while (true)
            {
                numberOfStudents = _dataReader.ReadIntNumber("Podaj liczbę uczestników kursu");
                if (numberOfStudents > validStudents.Count)
                {
                    ConsoleHelper.ShowMessage("W bazie nie ma wystarczającej liczny studentów, spróbuj jeszcze raz.");
                }
                else
                {
                    break;
                }

            }
            return numberOfStudents;
        }
        private List<StudentDto> SetListOfStudents(List<StudentDto> validStudents, int numberOfStudents)
        {
            validStudents.ForEach(x => ConsoleHelper.ShowMessage($"{x.Id} - {x.Name} {x.Surname}, PESEL: {x.Pesel}"));
            var choosenStudents = new List<StudentDto>();
                
            for (int i = 1; i <= numberOfStudents; i++)
            {
                var choosenStudentId = _dataReader.ReadIntNumber($"Podaj Id studenta nr {i}");
                var studentDt = validStudents.SingleOrDefault(x => x.Id == choosenStudentId);

                if (studentDt == null)
                {
                    ConsoleHelper.ShowMessage($"Nie zanleziono studenta o Id nr {choosenStudentId}");
                    i--;
                }
                else if (choosenStudents.Contains(studentDt))
                {
                    ConsoleHelper.ShowMessage($"Student o Id nr {choosenStudentId} został już wybrany");
                    i--;
                }
                else
                {
                    choosenStudents.Add(studentDt);
                }
            }
            return choosenStudents;
        }
    }
}
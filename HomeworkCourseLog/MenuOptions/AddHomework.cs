﻿using HomeworkCourseLog.MenuOptions.Interfaces;
using System.Collections.Generic;
using HomeworkCourseLog.TempDataHolders;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.BusinessLayer.BusinessModels;

namespace HomeworkCourseLog.MenuOptions
{
    class AddHomework : IMenuOption
    {
        private StudentService _studentService;
        private CourseLogService _courseLogService;
        private DataReader _dataReader;

        public AddHomework()
        {
            _studentService = new StudentService();
            _courseLogService = new CourseLogService();
            _dataReader = new DataReader();
            OptionDescription = "Dodaj pracę domową";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            if (ProgramTempData.GetActiveCourseLog() == null)
            {
                ConsoleHelper.ShowMessage("Najpierw wybierz kurs.");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return;
            }

            var courseStudents = _studentService.GetStudentsByCourseId(ProgramTempData.GetActiveCourseLog().Id);

            var homeworkDt = new HomeworkDto
            {
                MaxPointsPossible = _dataReader.ReadDoubleNumber("Podaj maksymalną ilość punktów do zdobycia"),
                CourseLog = ProgramTempData.GetActiveCourseLog()
            };
            homeworkDt.PointsOfStudents = AskForScore(courseStudents, homeworkDt.MaxPointsPossible);
            _courseLogService.AddHomework(homeworkDt);
        }

        private Dictionary<StudentDto, double> AskForScore(List<StudentDto> students, double maxPointsPossible)
        {
            var scores = new Dictionary<StudentDto, double>();
            foreach (var student in students)
            {
                scores.Add(student, _dataReader.ReadDoubleNumber(0, maxPointsPossible, $"Ile punktów zdobył {student.Name} {student.Surname}?"));
            }
            return scores;
        }
    }
}

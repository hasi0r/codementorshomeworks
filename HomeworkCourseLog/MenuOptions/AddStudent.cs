﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;

namespace HomeworkCourseLog.MenuOptions
{
    class AddStudent : IMenuOption
    {
        private StudentService _studentService;
        private DataReader _dataReader;

        public AddStudent()
        {
            _studentService = new StudentService();
            _dataReader = new DataReader();
            OptionDescription = "Dodaj studenta";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            var studentDt = new StudentDto
            {
                Pesel = AskUserForPeselNumber(),
                Name = _dataReader.ReadString("Podaj imię studenta/ki"),
                Surname = _dataReader.ReadString("Podaj nazwisko studenta/ki"),
                BirthDate = _dataReader.ReadDateTime("Podaj datę urodzenia studenta/ki"),
                Sex = _dataReader.ReadSex("Podaj płeć studenta/ki: ")
            };
            _studentService.AddStudent(studentDt);
        }
        private string AskUserForPeselNumber()
        {
            var existingStudents = _studentService.GetAllStudents();
            string pesel = null;
            bool isExist;
            do
            {
                isExist = false;
                pesel = _dataReader.ReadPesel("----------------------------\nPodaj nr PESEL studenta");

                foreach (var student in existingStudents)
                {
                    if (student.Pesel == pesel)
                    {
                        isExist = true;
                        ConsoleHelper.ShowMessage("Student o podanym numerze PESEL jest już dodany");
                    }
                }
            } while (isExist);
            return pesel;
        }
    }
}

﻿using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;
using HomeworkCourseLog.TempDataHolders;
using System.Linq;

namespace HomeworkCourseLog.MenuOptions
{
    class ChooseActiveCourseLog : IMenuOption
    {
        private CourseLogService _courseLogService;
        private DataReader _dataReader;

        public ChooseActiveCourseLog()
        {
            _courseLogService = new CourseLogService();
            _dataReader = new DataReader();
            OptionDescription = "Wybierz aktywny kurs";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            var validCourseLogs = _courseLogService.GetAllCourseLogs();
            if(validCourseLogs.Count() == 0)
            {
                ConsoleHelper.ShowMessage("Brak założonych kursów.");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return;
            }
            ConsoleHelper.ShowMessage("Założone kursy");
            validCourseLogs.ForEach(x => ConsoleHelper.ShowMessage($"{x.Id} - {x.CourseName}, prowadzony przez: {x.CourseLeader.Name} {x.CourseLeader.Surname}"));

            while (true)
            {
                var choosedId = _dataReader.ReadIntNumber("Podaj Id kursu do ustawienia");
                ProgramTempData.SetActiveCourseLog(validCourseLogs.SingleOrDefault(x => x.Id == choosedId));
                if (ProgramTempData.GetActiveCourseLog() != null)
                {
                    break;
                }
                ConsoleHelper.ShowMessage("Nie znaleziono kursu o podanym Id, proszę spróbować jeszcze raz.");
            }
        }
    }
}

﻿namespace HomeworkCourseLog.MenuOptions.Interfaces
{
    interface IMenuOption
    {
        string OptionDescription { get; }
        void RunOption();
    }
}

﻿using HomeworkCourseLog.MenuOptions.Interfaces;
using HomeworkCourseLog.TempDataHolders;
using System;
using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.ConsoleHelpers;

namespace HomeworkCourseLog.MenuOptions
{
    class PrintReport : IMenuOption
    {
        private BusinessLayer.Services.ReportService _reportService;
        public PrintReport()
        {
            _reportService = new BusinessLayer.Services.ReportService();
            OptionDescription = "Wyświetl raport";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            var courselog = ProgramTempData.GetActiveCourseLog();
            if (courselog == null)
            {
                ConsoleHelper.ShowMessage("Brak aktywnego kursu");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return;
            }
            var report = _reportService.GetReport(courselog);
            HeadRaport(report);
            StudentsRaport(report);
            ConsoleHelper.WaitForEnter();
        }

        public void HeadRaport(ReportDto report)
        {
            int tableWidth = 60;
            int separatorWidth = 61;
            TablePrinter.PrintLine(separatorWidth);
            TablePrinter.PrintRow(new string[] { report.CourseName }, tableWidth);
            TablePrinter.PrintLine(separatorWidth);
            TablePrinter.PrintRow(new string[] { "Próg obecności", "Próg z pracy domowej" }, tableWidth);
            TablePrinter.PrintLine(separatorWidth);
            TablePrinter.PrintRow(new string[] { report.PresencePercentMinimalScore.ToString() + "%", report.HomeworkPercentMinimalScore.ToString() + "%" }, tableWidth);
            TablePrinter.PrintLine(separatorWidth);
            TablePrinter.PrintRow(new string[] { "Prowadzący kurs", "Data rozpoczęcia kursu" }, tableWidth);
            TablePrinter.PrintLine(separatorWidth);
            TablePrinter.PrintRow(new string[] { $"{report.CourseLeader.Name} {report.CourseLeader.Surname}", report.StartDate.ToShortDateString() }, tableWidth);
            TablePrinter.PrintLine(separatorWidth);
        }
        public void StudentsRaport(ReportDto report)
        {
            int tableWidth = Console.BufferWidth - 2;
            TablePrinter.PrintLine(tableWidth);
            TablePrinter.PrintRow(new string[] { "Student", "Obecności", "Prace domowe" }, tableWidth);
            TablePrinter.PrintLine(tableWidth);

            int maxPresencePoints = report.ClassDaysCount;
            double maxHomeworkPoints = report.MaxHomeworksScorePossible;

            //students
            foreach (var student in report.StudentsList)
            {
                string[] rows = new string[3];
                rows[0] = $"{student.Name} {student.Surname}";

                //counting presence
                int actualPresence = report.StudentsPressence[student.Id];
                double presenceProcent = (maxPresencePoints != 0 ? (double)actualPresence / (double)maxPresencePoints * 100.0 : 0);
                string presencePasssed = Classified(presenceProcent, report.PresencePercentMinimalScore);
                rows[1] = $"{actualPresence.ToString()}/{report.ClassDaysCount}({presenceProcent:F}%) - {presencePasssed}";

                //counting homework points
                double earnedPoints = report.StudentsScores[student.Id];
                double homeworkPointsProcent = (maxHomeworkPoints != 0 ? earnedPoints / maxHomeworkPoints * 100.0 : 0);
                string homeworkPassed = Classified(homeworkPointsProcent, report.HomeworkPercentMinimalScore);
                rows[2] = $"{earnedPoints.ToString()}/{maxHomeworkPoints}({homeworkPointsProcent:F}%) - {homeworkPassed}";

                //writing rows on console
                TablePrinter.PrintRow(rows, tableWidth);
                TablePrinter.PrintLine(tableWidth);
            }
        }

        public string Classified(double actualPrecent, double minimalPrecent)
        {
            string classified;
            if (actualPrecent >= minimalPrecent)
            {
                classified = "Zaliczone";
            }
            else
            {
                classified = "Niezaliczone";
            }
            return classified;
        }
    }
}

﻿using HomeworkCourseLog.MenuOptions.Interfaces;
using HomeworkCourseLog.TempDataHolders;

namespace HomeworkCourseLog.MenuOptions
{
    class ProgramExit : IMenuOption
    {
        public ProgramExit()
        {
            OptionDescription = "Wyjdź z programu";
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            ProgramTempData.Exit = true;
        }
    }
}

﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog.MenuOptions
{
    class UpdateCourseLeader : IMenuOption
    {
        private DataReader _dataReader;
        private CourseLeaderService _courseLeaderService;
        private CourseLeaderDto _updatedCourseLeader;

        public UpdateCourseLeader()
        {
            OptionDescription = "Popraw dane prowadzącego";
            _dataReader = new DataReader();
            _courseLeaderService = new CourseLeaderService();
            _updatedCourseLeader = null;
        }

        public string OptionDescription { get; }
        public CourseLeaderService CourseLeaderService { get => _courseLeaderService; set => _courseLeaderService = value; }

        public void RunOption()
        {
            var validCourseLeaders = _courseLeaderService.GetAllCourseLeaders();
            if (validCourseLeaders.Count == 0)
            {
                ConsoleHelper.ShowMessage("Brak prowadzących w bazie");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return;
            }

            ConsoleHelper.ClearConsole();
            ConsoleHelper.ShowMessage("Wybierz prowadzącego, którego dane chcesz poprawić.");
            validCourseLeaders.ForEach(x => ConsoleHelper.ShowMessage($"{x.Id} - {x.Name} {x.Surname}"));
            _updatedCourseLeader = null;

            while (true)
            {
                var choosenCourseLeaderId = _dataReader.ReadIntNumber($"Podaj Id prowadzącego");
                _updatedCourseLeader = validCourseLeaders.SingleOrDefault(x => x.Id == choosenCourseLeaderId);
                if (_updatedCourseLeader != null)
                {
                    break;
                }
                ConsoleHelper.ShowMessage($"Nie zanleziono prowadzącego o Id nr {choosenCourseLeaderId}");
            }

            string[] options = { "Imię", "Nazwisko", "Zapisz zmiany i wróć do menu głównego", "Wyjdz bez zapisywania zmian" };
            var userMenu = new UserMenu(options);

            var exit = false;
            while (!exit)
            {
                ConsoleHelper.ClearConsole();
                var option = userMenu.AskUser("Wybierz co chcesz poprawić:");
                switch (option)
                {
                    case 1:
                        UpdateName();
                        break;
                    case 2:
                        UpdateSurname();
                        break;
                    case 3:
                        SaveChanges();
                        exit = true;
                        break;
                    case 4:
                        exit = true;
                        break;
                    default:
                        ConsoleHelper.ShowMessage("Niepoprawna opcja");
                        break;
                }
            }
        }

        private void SaveChanges()
        {
            _courseLeaderService.UpdateCourseLeader(_updatedCourseLeader);
            ConsoleHelper.ClearConsole();
        }

        private void UpdateSurname()
        {
            var newSurname = _dataReader.ReadString("Podaj nowe nazwisko");
            if (newSurname.Count() > 0)
            {
                _updatedCourseLeader.Surname = newSurname;
            }
        }

        private void UpdateName()
        {
            var newName = _dataReader.ReadString("Podaj nowe imię");
            if (newName.Count() > 0)
            {
                _updatedCourseLeader.Name = newName;
            }
        }
    }
}

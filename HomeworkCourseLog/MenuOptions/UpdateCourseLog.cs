﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;
using HomeworkCourseLog.TempDataHolders;
using System.Collections.Generic;
using System.Linq;

namespace HomeworkCourseLog.MenuOptions
{
    class UpdateCourseLog : IMenuOption
    {
        private CourseLogService _courseLogService;
        private DataReader _dataReader;
        private CourseLogDto _updatedCourseLog;
        private CourseLeaderService _courseLeaderService;
        private StudentService _studentService;
        private List<int> _removedStudents;

        public UpdateCourseLog()
        {
            OptionDescription = "Popraw kurs";
            _courseLogService = new CourseLogService();
            _dataReader = new DataReader();
            _courseLeaderService = new CourseLeaderService();
            _studentService = new StudentService();
            _removedStudents = null;
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            _removedStudents = new List<int>();
            var activeCourseLog = ProgramTempData.GetActiveCourseLog();
            if (activeCourseLog == null)
            {
                ConsoleHelper.ShowMessage("Brak aktywnego kursu");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return;
            }
            _updatedCourseLog = _courseLogService.GetCourseLogById(activeCourseLog.Id);

            string[] options = { "Nazwę", "Zmień prowadzącego", "Próg zaliczenia pracy domowej", "Próg obecności", "Wypisz kursanta z kursu", "Zapisz zmiany i wróć do menu głównego", "Wyjdz bez zapisywania zmian" };

            var userMenu = new UserMenu(options);

            var exit = false;
            while (!exit)
            {
                ConsoleHelper.ClearConsole();
                var option = userMenu.AskUser("Wybierz co chcesz poprawić:");
                switch (option)
                {
                    case 1:
                        UpdateCourseName();
                        break;
                    case 2:
                        UpdateCourseLeader();
                        break;
                    case 3:
                        UpdateHomeworkPercentMinimalScore();
                        break;
                    case 4:
                        UpdatePresencePercentMinimalScore();
                        break;
                    case 5:
                        RemoveStudent();
                        break;
                    case 6:
                        SaveChanges();
                        exit = true;
                        break;
                    case 7:
                        exit = true;
                        break;
                    default:
                        ConsoleHelper.ShowMessage("Niepoprawna opcja");
                        break;
                }
            }
        }


        private void SaveChanges()
        {
            _courseLogService.UpdateCourseLogData(_updatedCourseLog);
            _courseLogService.RemoveStudentsFromCourse(_updatedCourseLog.Id, _removedStudents);
            ProgramTempData.SetActiveCourseLog(_courseLogService.GetAllCourseLogs().SingleOrDefault(x => x.Id == _updatedCourseLog.Id));
            ConsoleHelper.ClearConsole();
        }

        private void RemoveStudent()
        {
            ConsoleHelper.ClearConsole();
            var studentsList = _studentService.GetStudentsByCourseId(ProgramTempData.GetActiveCourseLog().Id);
            ConsoleHelper.ShowMessage("Studenci na kursie:");

            foreach (var student in studentsList)
            {
                if (!_removedStudents.Contains(student.Id))
                {
                    foreach (var drawStudent in studentsList)
                    {
                        if (!_removedStudents.Contains(drawStudent.Id))
                        {
                            ConsoleHelper.ShowMessage($"{drawStudent.Id} - {drawStudent.Name} {drawStudent.Surname}");
                        }
                    }
                    while (true)
                    {
                        var removeStudentId = _dataReader.ReadIntNumber("Podaj nr studenta do usunięcia");
                        var removedStudent = studentsList.SingleOrDefault(x => x.Id == removeStudentId);

                        if (removedStudent != null)
                        {
                            _removedStudents.Add(removeStudentId);
                            break;
                        }
                        ConsoleHelper.ShowMessage($"Nie ma studenta o nr {removeStudentId}. Spróbuj ponownie");
                    }
                    break;
                }
                else
                {
                    ConsoleHelper.ShowMessage("Nie ma więcej kursantów");
                    ConsoleHelper.WaitForEnter();
                }
            }
        }

        private void UpdatePresencePercentMinimalScore()
        {
            _updatedCourseLog.PresencePercentMinimalScore = _dataReader.ReadDoubleNumber(0, 100, "Podaj nowy próg obecności(w procentach)");
        }

        private void UpdateHomeworkPercentMinimalScore()
        {
            _updatedCourseLog.HomeworkPercentMinimalScore = _dataReader.ReadDoubleNumber(0, 100, "Podaj nowy próg zaliczania prac domowyc(w procentach)");
        }

        private void UpdateCourseLeader()
        {
            ConsoleHelper.ClearConsole();
            var validCourseLeaders = _courseLeaderService.GetAllCourseLeaders();

            ConsoleHelper.ShowMessage("Dostępni prowadzący:");
            validCourseLeaders.ForEach(x => ConsoleHelper.ShowMessage($"{x.Id} - {x.Name} {x.Surname}"));

            CourseLeaderDto courseleaderDt;
            while (true)
            {
                int choosedCourseLeaderId = _dataReader.ReadIntNumber("Podaj Id Prowadzącego");
                courseleaderDt = validCourseLeaders.SingleOrDefault(x => x.Id == choosedCourseLeaderId);

                if (courseleaderDt != null)
                {
                    break;
                }
                ConsoleHelper.ShowMessage($"Nie ma prowadzącego o nr {choosedCourseLeaderId}. Spróbuj ponownie");
            }
            _updatedCourseLog.CourseLeader = courseleaderDt;
        }

        private void UpdateCourseName()
        {
            var newCourseName = _dataReader.ReadString("Podaj nową nazwę kursu");
            if (newCourseName.Count() > 0)
            {
                _updatedCourseLog.CourseName = newCourseName;
            }
        }
    }
}

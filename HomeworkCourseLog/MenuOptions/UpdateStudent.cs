﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;
using HomeworkCourseLog.BusinessLayer.Services;
using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkCourseLog.MenuOptions
{
    class UpdateStudent : IMenuOption
    {
        private DataReader _dataReader;
        private StudentService _studentService;
        private CourseLogService _courseLogService;
        private StudentDto _updatedStudent;
        private List<int> _removedCourses;

        public UpdateStudent()
        {
            OptionDescription = "Popraw dane studenta";
            _dataReader = new DataReader();
            _studentService = new StudentService();
            _updatedStudent = null;
            _removedCourses = null;
            _courseLogService = new CourseLogService();
        }

        public string OptionDescription { get; }

        public void RunOption()
        {
            _removedCourses = new List<int>();
            var validStudents = _studentService.GetAllStudents();
            if (validStudents.Count == 0)
            {
                ConsoleHelper.ShowMessage("Brak studentów w bazie");
                ConsoleHelper.WaitForEnter();
                ConsoleHelper.ClearConsole();
                return;
            }
            ConsoleHelper.ClearConsole();
            ConsoleHelper.ShowMessage("Wybierz studenta, którego dane chcesz poprawić.");
            validStudents.ForEach(x => ConsoleHelper.ShowMessage($"{x.Id} - {x.Name} {x.Surname}"));
            _updatedStudent = null;
            
            while (true)
            {
                var choosenStudentId = _dataReader.ReadIntNumber($"Podaj Id studenta");
                _updatedStudent = validStudents.SingleOrDefault(x => x.Id == choosenStudentId);
                if (_updatedStudent != null)
                {
                    break;
                }
                ConsoleHelper.ShowMessage($"Nie zanleziono studenta o Id nr {choosenStudentId}");
            }

            string[] options = { "Imię", "Nazwisko", "Datę urodzenia", "Wypisz studenta z kursu", "Zapisz zmiany i wróć do menu głównego", "Wyjdz bez zapisywania zmian" };

            var userMenu = new UserMenu(options);

            var exit = false;
            while (!exit)
            {
                ConsoleHelper.ClearConsole();
                var option = userMenu.AskUser("Wybierz co chcesz poprawić:");
                switch (option)
                {
                    case 1:
                        UpdateName();
                        break;
                    case 2:
                        UpdateSurname();
                        break;
                    case 3:
                        UpdateBirthDate();
                        break;
                    case 4:
                        RemoveStudentFromCourseLog();
                        break;
                    case 5:
                        SaveChanges();
                        exit = true;
                        break;
                    case 6:
                        exit = true;
                        break;
                    default:
                        ConsoleHelper.ShowMessage("Niepoprawna opcja");
                        break;
                }
            }

        }

        private void SaveChanges()
        {
            _studentService.UpdateStudentData(_updatedStudent);
            _studentService.RemoveCourseLogFromStudent(_updatedStudent.Id, _removedCourses);
             ConsoleHelper.ClearConsole();
        }

        private void RemoveStudentFromCourseLog()
        {
            ConsoleHelper.ClearConsole();
            var courseLogsList = _courseLogService.GetCourseLogsByStudentId(_updatedStudent.Id);
            ConsoleHelper.ShowMessage("Kursy, na które uczęszcza student:");

            foreach (var course in courseLogsList)
            {
                if (!_removedCourses.Contains(course.Id))
                {
                    foreach (var drawCourse in courseLogsList)
                    {
                        if (!_removedCourses.Contains(drawCourse.Id))
                        {
                            ConsoleHelper.ShowMessage($"{drawCourse.Id} - {drawCourse.CourseName}");
                        }
                    }
                    while (true)
                    {
                        var removedCourseId = _dataReader.ReadIntNumber("Podaj nr kursu");
                        var removedCourseLog = courseLogsList.SingleOrDefault(x => x.Id == removedCourseId);

                        if (removedCourseLog != null)
                        {
                            _removedCourses.Add(removedCourseId);
                            break;
                        }
                        ConsoleHelper.ShowMessage($"Nie ma kursu o nr {removedCourseId}. Spróbuj ponownie");
                    }
                    break;
                }
                else
                {
                    ConsoleHelper.ShowMessage("Student nie uczęszcza na więcej kusów");
                    ConsoleHelper.WaitForEnter();
                }
            }
        }

        private void UpdateBirthDate()
        {
            var newBirthDate = _dataReader.ReadDateTime("Podaj nową datę urodzenia");
            _updatedStudent.BirthDate = newBirthDate;
        }

        private void UpdateSurname()
        {
            var newSurname = _dataReader.ReadString("Podaj nowe nazwisko");
            if (newSurname.Count() > 0)
            {
                _updatedStudent.Surname = newSurname;
            }
        }

        private void UpdateName()
        {
            var newName = _dataReader.ReadString("Podaj nowe imię");
            if (newName.Count() > 0)
            {
                _updatedStudent.Name = newName;
            }
        }
    }
}

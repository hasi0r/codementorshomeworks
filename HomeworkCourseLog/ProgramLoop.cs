﻿using HomeworkCourseLog.ConsoleHelpers;
using HomeworkCourseLog.MenuOptions.Interfaces;
using HomeworkCourseLog.MenuOptions;
using HomeworkCourseLog.TempDataHolders;

namespace HomeworkCourseLog
{
    internal class ProgramLoop
    {
        public void Run()
        {
            IMenuOption[] options1 = { new AddStudent(), new AddCourseLeader(), new AddCourseLog(), new ChooseActiveCourseLog(),
                                       new AddClassDay(),new AddHomework(),  new PrintReport(),
                                       new UpdateCourseLog(), new UpdateStudent(), new UpdateCourseLeader(),
                                       new ProgramExit() };

            var userMenu = new OptionMenu(options1);
            ProgramTempData.Exit = false;

            while (!ProgramTempData.Exit)
            {
                ConsoleHelper.ClearConsole();
                int headWidth = 61;
                var activatedCourseName = (ProgramTempData.GetActiveCourseLog() != null ? ProgramTempData.GetActiveCourseLog().CourseName : "brak wybranego kursu");
                TablePrinter.PrintLine(headWidth);
                TablePrinter.PrintRow(new string[] { "Aktywny kurs:", activatedCourseName }, headWidth);
                TablePrinter.PrintLine(headWidth);
               
                var option = userMenu.AskUser("Wybierz Opcję:");
                options1[option - 1].RunOption();
            }
        }
    }
}

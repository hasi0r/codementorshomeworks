﻿using HomeworkCourseLog.BusinessLayer.BusinessModels;

namespace HomeworkCourseLog.TempDataHolders
{
    static class ProgramTempData
    {
        static private CourseLogDto _activeCourseLog;

        public static bool Exit { get; set; }

        public static void SetActiveCourseLog( CourseLogDto newCourseLogDt)
        {
            _activeCourseLog = newCourseLogDt;
        }
        public static CourseLogDto GetActiveCourseLog()
        {
            return _activeCourseLog;
        }
    }
}
